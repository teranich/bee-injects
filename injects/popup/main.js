(function($) {
    $(function() {
        setTimeout(function() {
            console.warn('Experiment has loaded');
            $('.content-block.tariff-description div p img').parents('p').hide();
            console.warn('Experiment has started!');
            var pre = 'http://static.beeline.ru/upload/images/vse-family/img/';
            var tartif = window.itemDetails.split(';');
            tartif = tartif.length > 0 ? tartif[0] : 'none';
            var tData = {
                'Всё за 200': 'block;block;none',
                'Всё за 400': 'block;block;none',
                'Всё за 600': 'block;block;block',
                'Всё за 900': 'block;block;block',
                'Всё за 1500': 'block;block;block',
                'Всё за 2700': 'block;block;block',
                'none': 'none;none;none'
            }
            var displays = (tData[tartif]).split(';');
            var data = {
                name0: pre + 'AB_SEB-03.jpg',
                name1: pre + 'AB_SEB-04.jpg',
                name2: pre + 'AB_SEB-05.jpg',
                d0: displays[0],
                d1: displays[1],
                d2: displays[2]
            }
            var html = tmpl(EXP_TEMPLATES['main'], data);
            $(html).insertAfter('#otherPaySysTariffSwitcher');
            var rest = QA.Beeline.Popup.close;
            QA.Beeline.Popup.close = (function() {
                $('.popup-content').css({
                    'width': '',
                    'margin': '',
                    'height': ''
                })
                $('.popup-close').off('click', QA.Beeline.Popup.close);

                rest();
            });
            $('#exp-4g').click(function() {
                QA.Beeline.Popup.show($(EXP_TEMPLATES['popup']));
                $('.popup-content').css({
                    'width': '254px',
                    'margin': 'auto',
                    'height': '415px'
                });

                $('.popup-close').click(QA.Beeline.Popup.close);
                $("#exp-popup-btn").beebutton({
                    id: '',
                    type: 'button',
                    title: 'Вернуться к описанию',
                    css: '',
                    click: 'QA.Beeline.Popup.close',
                    image: '',
                    disabled: false,
                    isGray: false,
                    url: '',
                    visible: true,
                    oninit: 'false'
                });
                var y = window.scrollY;
                var x = window.scrollX;
                window.scrollTo(x, y + 10);
                window.scrollTo(x, y);
            });

            QA.Beeline.Popup.init()
        }, 0);
    });
})(jQuery);
