(function($) {
    $(function() {
        //setTimeout(function() {
        function GetReplText(AInputText, AReplaceText) {
            var VRegExp = new RegExp(/Сколько это стоит\?/);
            var VResult = AInputText.replace(VRegExp, AReplaceText);
            return VResult;
        }

        function changes(text) {
            var elements = $(GetReplText(text, 'Один тариф для всей семьи'));
            var el = elements.find('.folded')[1]
            var price = /[0-9]+/.exec(window.location.pathname); 
            el.innerHTML = tmpl(EXP_TEMPLATES['main'], {price: price});
            return elements;
        }

        console.warn('Experiment loaded');
        $.ajax({
                url: window.location.href,
                success: function(data) {
                    var serviceText = GetText(data)[0];
                    console.warn('Experiment ajax success');

                    function GetText(AInputText) {
                        var VRegExp = new RegExp(/<!-- #region rendering -->[^]+<!-- #endregion rendering -->/);
                        var VResult = VRegExp.exec(AInputText);
                        return VResult;
                    }
                    var adapt = changes(serviceText);
                    $('#Services1').remove();
                    adapt.insertAfter($('.tariff-services').first());
                    console.warn('Experiment end');
                }
            })
            //}, 0);
    });
})(jQuery);
