/*
 *http://moskva.beeline.ru/customers/products/mobile/tariffs/details/vse-za-900/
 */
document.getElementsByTagName('body')[0].style.visibility = "hidden";
(function($) {
    $(function() {
        $('#otherPaySysTariffSwitcher').empty();
        setTimeout(function() {
            console.warn('Experiment started!');
            $('#otherPaySysTariffSwitcher').children().empty();
            $('#otherPaySysTariffSwitcher').removeClass('custom-selecter');
            var path = window.location.pathname;
            var tarif = /vse-za-[0-9]+/.exec(path)[0]
                //var tarif = $('.crumbs.clearfix li').last().text(); 
            var data = {
                typeother: '',
                type400_200: 'none',
                text0: window.itemTitle + ' Предоплата',
                text1: window.itemTitle + ' Постоплата',
                text2: 'Попробуйте &laquo;Всё за 600&raquo;',
                textd2: 'У ' + tarif + ' нет варианта с постоплатой',
                tarif0: tarif,
                tarif1: tarif,
                tarif2: 'vse-za-600',
                b0: '',
                b1: '',
                b2: '',
                bd2: 'none'
            }

            // для vse-400 vse-600 нет постоплаты
            if (tarif === 'vse-za-200' || tarif === 'vse-za-400') {
                data.typeother = 'none';
                data.type400_200 = '';
                var tarifname = tarif === 'vse-za-200' ? 'Всё за 200' : 'Всё за 400';
                data.textd2 = 'У &laquo;' + tarifname + '&raquo; нет варианта с постоплатой';


                //data.text2 = 'Нет варианта с постоплатой';
                //data.textd2 = 'Попробуйте "Всё за 600';
                ////data.b0 = 'none';
                //data.b1 = 'none';
                //data.b2 = 'none';
                //data.bd2 = 'block';

            }
            var html = tmpl(EXP_TEMPLATES['main'], data);
            $('#otherPaySysTariffSwitcher').append(html);
            if (tarif === 'vse-za-200' || tarif === 'vse-za-400') {
                //$('.exp-item').hide();
                $('.exp-card-link').css({
                    'color': '#c8c8c8'
                })
            }

            $('a[href="' + path + '"]').parents('.sub-nav-item').addClass('active')
            $('#otherPaySysTariffSwitcher').show();
            document.getElementsByTagName('body')[0].style.visibility = "visible";
        });
    });
})(jQuery);
