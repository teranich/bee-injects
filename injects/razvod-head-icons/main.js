//http://moskva.beeline.ru/customers/products/mobile/tariffs/index/cellphone/
document.getElementsByTagName('body')[0].style.visibility = "hidden";
var html = tmpl(EXP_TEMPLATES['main'], {});
(function($) {
    console.warn('Experiment has started!');
    $(function() {

        var interval = window.setInterval(function() {
            var info = $('.info').first();
            if (info.length) {
                $(html).insertAfter(info);
                document.getElementsByTagName('body')[0].style.visibility = "visible";
                clearInterval(interval);
            }
        }, 1)
    });
})(jQuery);
