(function($) {
    console.warn('Experiment has started!');
    //var styles = ['<styles>.content-block {width: 100%;} .tariffs.base-tariffs {width: 100%;} .tariffs{width: 100%;} .help-services{margin-right: 0px;}</styles>']
    //$('head').append(styles);
    var single = false;

    function run(cb) {
        cb && cb();
        $(document).ajaxComplete(function(e, o) {
            var parsed;
            try {
                parsed = JSON.parse(o.responseText);
            } catch (e) {
                parsed = {};
            }
            var go = (typeof parsed.View === 'string' ? parsed.View.indexOf('content-block') > -1 : false);
            if (!single && go) {
                console.warn('ajax loading');
                cb && cb();
                single = true;
            }

        });
    }

    function main() {
        //setTimeout(function() {
        $('.content-block').css({
            width: '100%'
        });
        $('.tariffs.base-tariffs').css({
            width: '100%'
        });
        $('.sidebar').hide();
        $('.tariffs').css('width', '100%');
        $('.help-services').css('margin-right', '0px');
        //}, 0);
    };

    run(main);

})(jQuery);
