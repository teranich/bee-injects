document.getElementsByTagName('body')[0].style.visibility = "hidden";
setTimeout(function() {
    document.getElementsByTagName('body')[0].style.visibility = "visible";
}, 2000);

(function($) {

    window.exp_once = false;
    console.warn('Experiment has been started!');

    function change() {
        if (window.exp_once) {
            return;
        }
        window.exp_once = true;
        $('.calculator.clearfix.tab-switcher-content').remove();
        $('.tab-tail').remove();
        document.getElementsByTagName('body')[0].style.visibility = "visible";
    }
    $(function() {

        setTimeout(function() {
            console.warn('experiment: timeout', window.exp_once);
            change();
        }, 3000);
        $(document).ajaxSuccess(function(e, xhr, settings) {

            var json = JSON.parse(xhr.responseText) || '';
            var i = json.View ? json.View.indexOf('tariffDeviceButtons') : -1;
            if (i > -1) {

                window.exp_once = false;
                change();
            }
        });
    });
})(jQuery);
