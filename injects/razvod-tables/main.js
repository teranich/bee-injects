(function($) {
    var single = false;

    console.warn('Experiment has started!');
    function run(cb) {
        cb && cb();
        $(document).ajaxComplete(function(e, o) {
            var parsed;
            try {
                parsed = JSON.parse(o.responseText);
            } catch (e) {
                parsed = {};
            }
            var go = (typeof parsed.View === 'string' ? parsed.View.indexOf('content-block') > -1 : false);
            if (!single && go) {
                console.warn('ajax loading');
                cb && cb();
                single = true;
            }

        });
    }

    function main() {
        setTimeout(function() {
            $('head').append(EXP_TEMPLATES['styles']);
            var tables = $('.card-group-item table');

            tables.each(function(i, item) {
                var tr = $(item).find('tr');
                tr.each(function(i, item) {
                    var td = $(item).find('td:not(:has(span))');
                    var text = td.eq(0).text();
                    var rtext = /[0-9]+[^]Гб/.exec(text);
                    if (i > 0 && rtext) {
                        var v0 = rtext ? rtext[0] : text;
                        var v2 = (td.eq(2).text()).trim();
                        var v1 = (td.eq(1).text()).trim() + ' / ' + v2;
                        var disp = 'block';
                        var parent = $(td).eq(1).parent();
                        var firstTd = parent.find('td').first();
                        if (v2.length === 0) {
                            v1 = '';
                            disp = 'none';
                        }

                        var usecls = (firstTd.find('.share-icon').length) ? 'exp-share-icon' : 'none';

                        var data = {
                            v0: v0,
                            v1: v1,
                            display: disp,
                            usecls: usecls
                        }

                        var html = tmpl(EXP_TEMPLATES['td'], data).trim();
                        td.remove();
                        $(html).insertAfter(firstTd);
                    }
                });

            });
        }, 0);
    }

    run(main);
    window.MAIN = main;

})(jQuery);
