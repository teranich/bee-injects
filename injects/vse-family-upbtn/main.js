//http://arkhangelsk.beeline.ru/customers/vse-family/
document.getElementsByTagName('body')[0].style.visibility = "hidden";
(function($) {
    console.warn('Experiment has been started!');
    $('head').append(EXP_TEMPLATES['styles']);
    $(function() {
        function findPos(obj) {
            $('html, body').animate({
                scrollTop: $(obj).offset().top
            }, 300);
            //var curtop = 0;
            //if (obj.offsetParent) {
                //do {
                    //curtop += obj.offsetTop;
                //} while (obj = obj.offsetParent);
                //return [curtop];
            //}
        }
        $('.landing-block.fifth').insertAfter('.landing-block.first');
        $('.landing-block.fifth').attr('id', 'exp-id');
        $('<div class="landing-block "><span class="exp-back dynamic">Выбрать тариф</span></div>').insertAfter('.landing-block.fourth');
        $('.exp-back').on('click', function() {
            console.warn('experiment: click');
            window.scroll(0, findPos(document.getElementById('exp-id')));
        });
        document.getElementsByTagName('body')[0].style.visibility = "visible";
    });
})(jQuery);
