//http://moskva.beeline.ru/customers/products/mobile/tariffs/details/vse-za-600/
(function($) {
    console.warn('Experiment has started!');
    var single;
    var TEXT = 'При переходе на тариф сразу спишется ежемесячная плата за<br/>весь месяц.';
    $('.button label').on('click', function() {

        if (!single) {
            single = true;
            var interval = setInterval(function() {
                if ($('.popup fieldset').length) {
                    modify();
                    clearInterval(interval);
                }
            }, 0);

        }
    });

    function modify() {
        console.log('modify');

        $('<div>' + TEXT + '</div>')
            .css({
                'font-size': '13px',
                'color': '#282828',
                'text-align': 'left',
                'display': 'table',
                'margin': '0 auto',
                'font-family': "'officinaserifcregular', sans-serif"
            })
            .insertAfter($('#popup-content h2'));
    }
})(jQuery);
