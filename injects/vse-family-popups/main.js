//http://arkhangelsk.beeline.ru/customers/vse-family/
(function($) {
    console.warn('Experiment has been started!');
    $(function() {
        var script = document.createElement('script');
        script.src = '/pages/mobiletariffs/details';
        document.head.appendChild(script);
        var links = $('table .btn-link')
        links.each(function(item) {
            var href = $(this).attr('href');
            var tarif = /vse-za-[0-9]+/.exec(href);
            if (!tarif && href.indexOf('/sovsemvse') > -1) {
                tarif = 'sovsemvse';
            }
            $(this).replaceWith('<div data-url=' + href + ' class="exp-btn btn-link" data-id=' + tarif + '>' + $(this).text() + '</div>')
        });

        function getInit(url, cb) {
            console.warn('URL:', url);
            $.ajax({

                url: url
            }).done(function(data) {
                cb && cb(data);
            })
        }

        script.onload = function() {
            $('.exp-btn').on('click', function() {
                var tarif = $(this).attr('data-id');
                console.warn('experiment: clicked', tarif);
                var url = $(this).attr('data-url');

                getInit(url, function(data) {
                    var config = /QA.Beeline.Pages.MobileTariff.Bill.init\([^\)]+/.exec(data)[0] + ')';
                    eval(config);
                    QA.Beeline.Popup.init();
                    QA.Beeline.FindError.init("/popup/FindError");
                    QA.Beeline.Pages.MobileTariff.Bill.connect()

                })

            });



        }


    });
})(jQuery);
