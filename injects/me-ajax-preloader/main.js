
//document.getElementsByTagName('body')[0].style.visibility = "hidden";
function Preload(element) {
    var loading = $("<style>\n" +
        "    .exp-load {\n" +
        "        position: absolute;\n" +
        "        background: white;\n" +
        "        width: 100%;\n" +
        "        height: 150%;\n" +
        "        z-index: 11;\n" +
        "        margin: -18px 0px 0px 0px;\n" +
        "    }\n" +
        "</style>\n" +
        "<div class=\"exp-load\">\n" +
        "    <div class=\"load\" style=\"\">\n" +
        "        &nbsp;\n" +
        "    </div>\n" +
        "</div>\n" +
        "");
    $(element).prepend(loading);
    this.inst = loading
    console.warn('Experiment has started!');
}
Preload.prototype.start = function() {
    $(this.inst).show();
}
Preload.prototype.stop = function() {
    $(this.inst).hide();
}
$(function() {
    prelpad = new Preload('#otherPaySysTariffSwitcher');
})
