/*
 *http://moskva.beeline.ru/customers/products/mobile/tariffs/details/vse-za-900/
 */
(function($) {
    $(function() {
        $('#otherPaySysTariffSwitcher').hide();
        setTimeout(function() {
            console.warn('Experiment started!');
            $('#otherPaySysTariffSwitcher').children().hide();
            $('#otherPaySysTariffSwitcher').removeClass('custom-selecter');
            var data = {
                text0: window.itemTitle + ' предоплата',
                text1: window.itemTitle + ' постоплата',
            }
            var html = tmpl(EXP_TEMPLATES['main'], data);
            $('#otherPaySysTariffSwitcher').append(html);
            var path = window.location.pathname;
            $('a[href="' + path + '"]').parents('.sub-nav-item').addClass('active')
            $('#otherPaySysTariffSwitcher').show();
        });
    });
})(jQuery);
