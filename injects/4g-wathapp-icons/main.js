(function($) {
    console.warn('Experiment has loaded');
    $(function() {
        setTimeout(function() {
            console.warn('Experiment has started!');
            var pre = 'http://static.beeline.ru/upload/images/vse-family/img/';
            var tartif = window.itemDetails.split(';');
            tartif = tartif.length > 0 ? tartif[0] : 'none';
            var tData = {
                'Всё за 200': 'block;block;none',
                'Всё за 400': 'block;block;none',
                'Всё за 600': 'block;block;block',
                'Всё за 900': 'block;block;block',
                'Всё за 1500': 'block;block;block',
                'Всё за 2700': 'block;block;block',
                'none': 'none;none;none'
            }
            $('.content-block.tariff-description div p img').parent().hide();
            var displays = (tData[tartif]).split(';');
            var data = {
                name0: pre + 'AB_SEB-03.jpg',
                name1: pre + 'AB_SEB-04.jpg',
                name2: pre + 'AB_SEB-05.jpg',
                d0: displays[0],
                d1: displays[1],
                d2: displays[2]
            }
            var html = tmpl(EXP_TEMPLATES['main'], data);
            $(html).insertAfter('#otherPaySysTariffSwitcher')
        }, 0);
    });
})(jQuery);
