//http://moskva.beeline.ru/customers/products/mobile/tariffs/
document.getElementsByTagName('body')[0].style.visibility = "hidden";
(function($) {
    console.warn('Experiment has started!');

    function fn() {
        var x = $('.tariffs.base-tariffs .card').last();
        var y = $('#Addition1 .tariffs .card').first();
        var yParent = y.parent();
        x.parent().append(y);
        yParent.prepend(x);
    }
    $(function() {
        //fn();
        var interval = window.setInterval(function() {
            var t = $('#Addition1 .tariffs .card');
            if (t.length) {
                fn()
                document.getElementsByTagName('body')[0].style.visibility = "visible";
                clearInterval(interval);
            }
        }, 1)
    });
})(jQuery);
