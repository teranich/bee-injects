function cat_collection() {
    var collection;
    return collection = [
        [".main-header-widget a", null, ["openReclame", "onWidgetClick"]],
        [".container .button", "a", ["conversionClick"]],
        [".banners-content .carousel .active", "h3", ["openReclame", "onBannerClick"]],
        [".sidebar-widget-wrapper", "h3", ["openReclame", "onBannerClick"]],
        ["#paramGroups h4", "span", ["hiddenInteraction"]],
        ["#paramGroups .show-all", "em", ["hiddenInteraction"]],
        [".tariff-description .button", "label", ["conversionClick"]],
        ["#Services1 h4", "span", ["hiddenInteraction"]],
        ["#Services1 .show-all", "em", ["hiddenInteraction"]],
        ["#TopAccordion h4", "span", ["hiddenInteraction"]],
        ["#TopAccordion .show-all", "em", ["hiddenInteraction"]],
        ["#pack-block h4", "span", ["hiddenInteraction"]],
        [".service-description .content-block .button", "label", ["conversionClick"]],
        ["#services1_Tab", "span", ["hiddenInteraction", "Еще услуги / services"]],
        ["#tariffs1_Tab", "span", ["hiddenInteraction", "Еще тарифы / tariffs"]],
        ["#Addition1 h4", "span", ["hiddenInteraction"]],
        ["#fix-sidebar .button", "label", ["conversionClick"]],
        ["#fix-sidebar .form-note", "span", ["conversionClick"]],
        [".toggle li:not(.active)", "a,span", ["openTab"]],
        ["#private_control_connection", null, ["openTab"]],
        ["#sms_connection", null, ["openTab"]],
        ["#ussd_connection", null, ["openTab"]],
        ["#popup-content .button", "label,a", ["conversionClick"]],
        [".catalog-filter li", "label", ["checkInteraction"]],
        ["#nav a", null, ["commonClick"]],
        [".bill-ribbon .button-detail", ".button-detail-text", ["commonClick"]],
        [".top-navigation-menu .sub-nav li:nth-of-type(3):not(.active)", "a", ["openTab", "Сообщения"]],
        [".top-navigation-menu .sub-nav li:not(.active)", "a", ["openTab"]],
        ["#AccountSelector li", null, ["commonClick", "switchAccount"]],
        ["#MainPageStatusLink", null, ["openHidden"]],
        ["#MainPageStatusDiv .button", "label", ["commonClick"]],
        ["#MainPageStatusDiv .hide-ctn-dialog-button i", null, ["closeHidden"]],
        [".account-info-aside .updateBalance", null, ["commonClick"]],
        [".account-info-aside a:not(.updateBalance)", null, ["conversionClick"]],
        [".profile-info .header-row", "h3", ["checkInteraction"]],
        [".contents-messages .arrow-down", "span", ["openHidden"], null, [/№.*|[0-9]*/g]],
        [".contents-messages .arrow-right", "span", ["closeHidden"], null, [/№.*|[0-9]*/g]],
        [".block-notification .arrow-down", "span", ["openHidden"], null, [/№.*|[0-9]*/g]],
        [".block-notification .arrow-right", "span", ["closeHidden"], null, [/№.*|[0-9]*/g]],
        ["#MobileNotificationProfileWidget .inform-events li", "label", ["checkInteraction"]],
        [".sso-settings-page .add-link", "a", ["commonClick"]],
        [".fast-pay .ButtonSend:not(.disabled)", "label", ["conversionClick"]],
        ["._oplata_conditions li", "label", ["checkInteraction"]],
        [".submit-or-cancel .button", "label", ["conversionClick"]],
        [".submit-or-cancel .dynamic", null, ["rejectClick"]],
        [".tab-switcher-content li", "label", ["checkInteraction"]],
        [".help-sidebar li:not(.active) span", null, ["openTab"]],
        [".help-content .button:not(.disabled)", null, ["conversionClick"]],
        [".help-content .btn-link", null, ["conversionClick"]],
        ["#submitLabel", null, ["conversionClick"]],
        [".sub-nav li", "a", ["openTab"]],
        [".download a", null, ["downloadInteraction"]],
        [".button:not(.disabled)", "label,a", ["commonClick"], null, [/ [0-9]*/gm]],
        [".filter li", "span", ["openTab"]],
        ["#fix-sidebar .dynamic", null, ["commonClick"]],
        [".lk-mobile-account", null, ["commonClick", "Мой Билайн"]],
        [".cancel-remove-button", "span", ["commonClick"]],
        ["a", null, ["commonClick"]]
    ]
}

$(function() {
            $(document).ajaxComplete(function(event, xhr, settings) {
                var ajax_collection = [
                    /checkconnectstatus/,
                    /tariffs\/connect/,
                    /services\/(disconnect|connect)/,
                    /checkconnectservicestatus/,
                    /CodeConfirmation/
                ];
                console.log("Testing settings URL: " + settings.url);
                if (/SMSConfirmation/.test(settings.url)) {
                    var request = $.parseJSON(xhr.responseText);
                    if (request.IsSucceeded) {
                        var heading = bee.analytics.text(document.querySelectorAll("#popup-content fieldset > h2")),
                            content = bee.analytics.text(document.querySelectorAll("#popup-content .product-popup-text, #popup-content h4", [0, 32]));
                        sendEvent("ajaxComplete", heading, content);
                        return;
                    }
                } else {
                    for (var i = 0; i < ajax_collection.length; i++) {
                        if (ajax_collection[i].test(settings.url)) {
                            var request = $.parseJSON(xhr.responseText);
                            if (request.IsSucceeded) {
                                var heading = bee.analytics.text(document.querySelectorAll("#popup-content h2")),
                                    content = bee.analytics.text(document.querySelectorAll("#popup-content .product-popup-text, #popup-content h4", [0, 32]));
                                sendEvent("ajaxComplete", heading, content);
                                return;
                            }
                        }
                    }
                }
            });
