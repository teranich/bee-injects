var bee = {} || bee;
bee.analytics = {} || bee.analytics;

bee.analytics.text = function(collection, characters, replace) {
    console.warn('bee.analytics.text', arguments)
    var selector, text = "";
    if (collection.length != undefined) {
        for (var i = 0; collection[i]; i++) {
            selector = collection[i];
            // Get the text from text nodes and CDATA nodes
            if (selector.nodeType === 3 || selector.nodeType === 4) {
                text += selector.nodeValue;
                // Traverse everything else, except comment nodes
            } else if (selector.nodeType !== 8) {
                text += bee.analytics.text(selector.childNodes);
            }
        }
    } else {
        selector = collection;
        text += selector.innerText || selector.textContent;
    }

    if (replace != undefined) {
        var replacement,
            pattern = replace[0];
        (replace[1] != undefined) ? replacement = replace[1]: replacement = "";
        if (pattern.test(text)) text = text.replace(pattern, replacement);
        console.log("Pattern '" + pattern + "' replaced with '" + replacement + "'");
    }
    text = text.trim();
    text = text.replace(/\r\n|\n|\r|\s/gm, " ");
    text = text.replace(/^(\s|\u00A0)+|(\s|\u00A0)+$/g, " ");
    text = text.replace(/\s+(?=\s)/g, " ");
    if (characters != undefined && characters != null) {
        if (characters[1] == undefined) text = text.substring(characters[0]);
        else text = text.substring(characters[0], characters[1]);
    }
    return text;
};

bee.analytics.geteventdata = function(triggerData, trigger, target) {
    var text, textSelector,
        ctrl = triggerData[2][0],
        place = triggerData[0].replace(/\.|\#|\>/gm, "");

    if (triggerData[2][1] == undefined || ctrl == "openReclame") {
        if (triggerData[1] != "this" && triggerData[1] != null) {
            text = bee.analytics.text(trigger.querySelectorAll(triggerData[1]), triggerData[3], triggerData[4])
        } else {
            text = bee.analytics.text(target, triggerDat[], triggerData[4]);
        }
        console.log("Select text from " + triggerData[1]);
    } else {
        text = triggerData[2][1];
        console.log("Taken text from triggerData[2][1]");
    }

    if (ctrl == "openReclame") {
        openReclame(trigger, triggerData, text);
    } else if (ctrl == "hiddenInteraction") {
        hiddenInteraction(trigger, triggerData, text, place);
    } else if (ctrl == "checkInteraction") {
        checkInteraction(trigger, triggerData, text, place);
    } else {
        simpleClick(trigger, triggerData, text, place);
    }
    console.log("Choosen controller equals " + ctrl);
};

function onClickCheckCollection(e) {
    if (typeof cat_collection == "function") {
        var collection = cat_collection();
    } else {
        sendEvent("error", "GTM-K2RV2J CAT", "Document has no cat_collection function");
        return;
    }
    outer: for (var trigger = e.target; trigger && trigger != this; trigger = trigger.parentNode) {
        for (var c = 0; c < collection.length; c++) {
            if (typeof trigger.matches == 'function' && trigger.matches(collection[c][0])) {
                bee.analytics.geteventdata(collection[c], trigger, e.target);
                break outer;
            }
        }
    }
}

function simpleClick(trigger, triggerData, text, place) {
        var eventAction = text + " / " + place;
        sendEvent(triggerData[2][0], eventAction);
    }
    //["#tariffs1_Tab", "span", ["hiddenInteraction", "Еще тарифы / tariffs"]]
    //function sendEvent(eventCategory, eventAction, eventContext) {

function openReclame(trigger, triggerData, text) {
    var eventContext = getNth(trigger) + " / " + text;
    sendEvent("openReclame", triggerData[2][1], eventContext);
}

function hiddenInteraction(trigger, triggerData, text, place) {
    var eventAction,
        classes = trigger.className;
    (triggerData[2][1] != undefined) ? eventAction = triggerData[2][1]: eventAction = text + " / " + place;
    (/opened/).test(classes) ? sendEvent("openHidden", eventAction) : sendEvent("closeHidden", eventAction)
}

function checkInteraction(trigger, triggerData, text, place) {
    var eventAction = text + " / " + place,
        checkbox = trigger.querySelector(".checkbox") || trigger.querySelector(".checkbox-slide") || trigger,
        classes = checkbox.className;
    if (!/inactive/.test(classes)) {
        if (/checkbox-slide/.test(classes)) {
            (!/checked/.test(classes)) ? sendEvent("checkOn", eventAction): sendEvent("checkOff", eventAction)
        } else {
            (/checked/.test(classes)) ? sendEvent("checkOn", eventAction): sendEvent("checkOff", eventAction)
        }
    }
}

function getNth(element) {
    var sibbling = element;
    var n = 1,
        i = 1;
    while ((sibbling = sibbling.previousSibling) && i++) {
        if (sibbling.nodeType === Node.ELEMENT_NODE) n++
    }
    var nth = n;
    return nth;
}

function textTreatment(textSelector) {
    var text = "";
    for (var i = 0; i < textSelector.length; i++) {
        text += textSelector[i].innerText || textSelector[i].textContent;
    }
    if (text == undefined || text == "") {
        text = textSelector.innerText || textSelector.textContent;
    }
    if (text != undefined) {
        text = text.trim();
        text = text.replace(/\r\n|\n|\r|\s/gm, " ");
        text = text.replace(/\s+(?=\s)/g, " ");
        text = text.replace(/ +(?= )/g, " ");
    }
    return text;
}

function sendEvent(eventCategory, eventAction, eventContext) {
    var eventData = {
        "eventCategory": eventCategory,
        "eventAction": eventAction,
        "event": "GA_event",
        //"eventLabel": {
        //{
        //URL Host - Path
        //}
        //}
    }

    if (eventContext) eventData["eventContext"] = eventContext;

    dataLayer.push(eventData);
    console.log("dataLayer have been pushed with:\nec: " + eventCategory + "\nea: " + eventAction);
}

if (document.addEventListener && onClickCheckCollection) {
    document.addEventListener("click", onClickCheckCollection);
} else if (document.attachEvent && onClickCheckCollection) {
    document.attachEvent("onclick", onClickCheckCollection);
} else {
    sendEvent("error", "GTM-K2RV2J CAT", "CAT says: 'Document has no addEventListener or onClickCheckCollection function'");
}
