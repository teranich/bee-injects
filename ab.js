(function(cookie_name, healhcheck_url, health_timeout, load_timeout, insert_counter) {
    var getCookie = function(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    var setCookie = function(name, value, options) {
        value = encodeURIComponent(value);
        var updatedCookie = name + "=" + value;
        document.cookie = updatedCookie;
    }

    var hide = function() {
        document.write('<style id="wait">body {visibility: hidden;}</style>');
    }
    var show = function() {
        document.getElementsByTagName('body')[0].style.visibility = "visible";
    }
    
    var runIfHealthy = function(healhcheck_url, success, fail) {
        var isLoaded = undefined;
        var img = document.createElement('img');
        img.onload = function() {
            isLoaded = true;
        }
        img.onerror = function() {
            isLoaded = false;
        }
        img.src = healhcheck_url;
        var timer = 0;
        var interval  = setInterval(function(){
            timer++;
            if(timer >= (health_timeout  / 100) || isLoaded !== undefined) {
                if(isLoaded) {
                    clearInterval(interval);
                    success();
                } else {
                    fail();
                }
            }
        }, 100);
    }

    if (getCookie(cookie_name) == 'fail') {
        return;
    }
    hide();

    if (getCookie(cookie_name) == 'success') {
        var node = insert_counter();
        var timeout = setTimeout(function () {
            setCookie(cookie_name, 'fail');
            show();
        }, load_timeout);
        node.onload = function () {
            clearTimeout(timeout);
            
            //show();
        };
        node.onerror = function() {
            setCookie(cookie_name, 'fail');
            show();
        };
    } else {
        runIfHealthy(healhcheck_url, function() {
            var timeout = setTimeout(show, load_timeout);
            var node = insert_counter();
            node.onload = function () {
                setCookie(cookie_name, 'success');
                clearTimeout(timeout);
                show();
            };
            node.onerror =  function() {
                setCookie(cookie_name, 'fail');
                show();
            };
        }, function() {
            setCookie(cookie_name, 'fail');
            show();
        })
    }
})(
    "ab_netfail",
    "https://app.convertexperiments.com/sys/"+ Math.random() * 100000000 +"/img/ok.png",
    2000, // img timeout
    2000, // counter timeout
    (function() {
        var _conv_host = (("https:" == document.location.protocol) ? "https://d9jmv9u00p0mv.cloudfront.net" : "http://cdn-1.convertexperiments.com");
        var d = document, f = d.getElementsByTagName('script')[0],
        s = d.createElement('script');
        s.type = 'text/javascript'; s.async = true;
        s.src = (_conv_host + "/js/10014218-10013581.js");
        f.parentNode.insertBefore(s, f);
        return s;
    })
);
