var gulp = require('gulp');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var html2string = require('gulp-html2string');
var gulpFilter = require('gulp-filter');
var map = require('map-stream');
var path = require('path');
var fs = require('fs')
var connect = require('gulp-connect');
var request = require('request');
var log = function(file, cb) {
    console.log('patch:', file.path);
    cb(null, file);
};

var merge = require('merge-stream');
var path = require('path'),
    folders = require('gulp-folders');

gulp.task('web', function() {
  connect.server({
    root: ['.'],
    port: 8984,
    livereload: true
  });
});
