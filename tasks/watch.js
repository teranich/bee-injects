var gulp = require('gulp-param')(require('gulp'), process.argv);
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var html2string = require('gulp-html2string');
var gulpFilter = require('gulp-filter');
var path = require('path');
var livereload = require('gulp-livereload');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var buildCore = require('./build-core')

gulp.task('watch', ['web'], function() {
    livereload.listen();
    gulp.watch('injects/**', function(event) {
        var folder = path.dirname(event.path);
        return buildCore(folder, 'current.js')
            .pipe(gulp.dest('tmp'))
            .pipe(livereload());
    })
});
