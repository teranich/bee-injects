/*
 * Slack Upload
 *
 *  gulp slack --folder injects/test/ 
 * 
 * gulp slack:archive --all
 * gulp slack:archive --channel channel_name
 */
var log4js = require('log4js');
var logger = log4js.getLogger(' ');


var gulp = require('gulp-param')(require('gulp'), process.argv);
var map = require('map-stream');
var uglify = require('gulp-uglify');
var log = function(file, cb) {
    cb && cb(null, file);
};
var path = require('path'),
    folders = require('gulp-folders'),
    pathToFolder = 'injects/';

var buildCore = require('./build-core')
var fs = require('fs');
var token = 'xoxp-9216054388-9216068131-14085779792-8bf99e62d6';

var Slack = require('slack-node');

slack = new Slack(token);
var _ = require('lodash');

gulp.task('preprod', function(folder) {
    return buildCore(folder)
        .pipe(uglify())
        .pipe(gulp.dest('dist'))

});

gulp.task('slack', ['preprod'], function(folder, channel) {
    var chanelName = channel || path.basename(folder) || 'general';
    var filename = 'dist/' + path.basename(folder) + '.js'
    logger.debug('filename', filename, folder, chanelName)

    function findChannel(name) {
        return new Promise(function(resolve, reject) {
            slack.api('channels.list',{
                exclude_archived: true
            }, function(err, response) {
                if (err) {
                    reject(err)
                } else if (response.ok) {
                    var result = _.find(response.channels, function(item) {
                        //logger.debug(item.name.toUpperCase(), chanelName.toUpperCase());
                        return item.name.toUpperCase() === chanelName.toUpperCase();
                    })
                    result ?
                        resolve(result) :
                        reject('Find channel ' + name);
                } else {
                    reject(response)
                }
            });
        });
    }

    new Promise(function(resolve, reject) {
            return slack.api('channels.create', {
                token: token,
                name: chanelName
            }, function(err, response) {
                //logger.debug('before upload', arguments);
                if (err) {
                    reject(err)
                } else if (response.ok) {
                    resolve(response.channel)
                } else {
                    resolve(findChannel(chanelName));
                }
            });
        }).then(function(channel) {
            var Slack = require('node-slack-upload');
            var slack = new Slack(token);
            slack.uploadFile({
                file: fs.createReadStream(filename),
                filetype: 'js',
                //title: 'test',
                //initialComment: 'my comment newwww',
                channels: channel.id
            }, function(err, resp) {
                if (err) {
                    throw Error(err);
                } else {
                    return true;
                }
            });
        })
        .then(function () {
            logger.info('Done');
        })
        .catch(function(err) {
            logger.error(err);
        });
});

gulp.task('slack:archive', function(channel, all) {
    if (!(channel || all)) {
        return console.warn('Wrong arguments channel=%s all=%s', channel, all);
    }
    new Promise(function(resolve, reject) {
            slack.api('channels.list', {
                exclude_archived: true
            }, function(err, response) {
                if (err) {
                    reject(err)
                } else if (response.ok) {
                    var result;
                    if (all) {
                        result = _.map(response.channels, function(item) {
                            return item.id;
                        });
                    } else {
                        result = _.find(response.channels, function(item) {
                            return item.name === channel;
                        });
                        result = result ? [result.id] : result;
                    }
                    result ?
                        resolve(result) :
                        reject(result);
                } else {
                    reject(response)
                }
            });
        })
        .then(function(ids) {
            var result = _.map(ids, function(item) {
                var res;
                return new Promise(function(resolve, reject) {
                    slack.api('channels.archive', {
                        token: token,
                        channel: item
                    }, function(err, response) {
                        err ?
                            reject(err) :
                            resolve(response);
                    });
                });
            });
            return Promise.all(result);
        })
        .then(function(result) {
            console.log('Done: ', result)
        })
        .catch(function(err) {
            console.error(err);
        });

});
