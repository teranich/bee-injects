/*
 * BUILD
 */
var gulp = require('gulp-param')(require('gulp'), process.argv);
var concat = require('gulp-concat');
var map = require('map-stream');
var path = require('path');
var uglify = require('gulp-uglify');
var html2string = require('gulp-html2string');
var log = function(file, cb) {
    console.log('path:', file && file.path);
    cb && cb(null, file);
};
var rename = require("gulp-rename");

var path = require('path'),
    folders = require('gulp-folders'),
    pathToFolder = 'injects/';

var eventStream = require('event-stream');
module.exports = function(dir, filename) {
    folder = dir || pathToFolder + 'test';
    filename = filename || path.basename(folder) + '.js';

    console.log('folder: %s file: %s', folder, filename);
    var htmlStream = gulp.src(path.join(folder, '*.html'))
        .pipe(rename(function(path) {
            path.extname = '';
        }))
        .pipe(html2string({
            base: folder,
            createObj: true, // Indicate wether to define the global object that stores
            objName: 'EXP_TEMPLATES' // Name of the global template store variable
        }))
    var jsStream = gulp.src(path.join(folder, '*.js'))

    return eventStream.merge(htmlStream, jsStream)
        .pipe(concat(filename))

};
