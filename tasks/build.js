/*
 * BUILD
 */
var gulp = require('gulp');
var map = require('map-stream');
var uglify = require('gulp-uglify');
var log = function(file, cb) {
    cb && cb(null, file);
};
var path = require('path'),
    folders = require('gulp-folders'),
    pathToFolder = 'injects/';

var buildCore = require('./build-core')

gulp.task('build', folders(pathToFolder, function(folder) {
    return buildCore(path.join(pathToFolder, folder))
        .pipe(uglify())
        .pipe(gulp.dest('dist'))

}));
