/*
 * Upload Task
 */
var gulp = require('gulp-param')(require('gulp'), process.argv);
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var html2string = require('gulp-html2string');
var gulpFilter = require('gulp-filter');
var uglify = require('gulp-uglify');
var map = require('map-stream');
var path = require('path');
var fs = require('fs')
var request = require('request');
var log = function(file, cb) {
    console.log('patch:', file.path);
    cb(null, file);
};
process.env.INIT_CWD = process.cwd();

var html2string = require('gulp-html2string');
var map = require('map-stream');
var merge = require('merge-stream');
var path = require('path'),
    folders = require('gulp-folders');
// dir = injects/pathname

var host = process.argv.indexOf('-local') > -1 ? 'http://localhost:9000' : 'http://89.108.88.163:9000'
var URL = host + '/api/injects/upload'

//var FILENAME = path.normalize(__dirname + '/../dist/4upload.js');

function build(dir, filename) {
    console.log(' build arguments', arguments)
    return new Promise(function(resolve, reject) {

        var jsFilter = gulpFilter('**/*.js', {
            restore: true
        });
        var htmlFilter = gulpFilter('**/*.html', {
            restore: true
        });
        var _filename = filename || '../4upload.js';

        console.log('_filename', dir, _filename);
        return gulp.src(dir + '/**')
            .pipe(htmlFilter)
            .pipe(rename(function(path) {
                path.extname = '';
                return path;
            }))
            .pipe(html2string({
                base: dir,
                createObj: true, // Indicate wether to define the global object that stores
                objName: 'EXP_TEMPLATES' // Name of the global template store variable
            }))
            //.pipe(map(log))
            .pipe(concat(_filename))
            .pipe(gulp.dest('dist'))
            .pipe(htmlFilter.restore)
            .pipe(jsFilter)
            .pipe(concat(_filename))
            //.pipe(uglify())
            .pipe(gulp.dest('dist'))
            .pipe(jsFilter.restore)
            .on('finish', function() {
                console.log('end', arguments)
                resolve()
            })
            .on('error', reject)
    })
}

gulp.task('upload', function(dir) {

    var formData = {};
    if (typeof dir === 'string') {

        var mkInfo = JSON.parse(fs.readFileSync(dir + '/make.json', 'utf8'));
        var filename = mkInfo.name || path.basename(dir) + '.js';
        console.log('info ', dir, mkInfo, filename)
        build(dir, filename)
            .then(upload({
                name: mkInfo.name || 'noname',
                url: mkInfo.url || '',
                info: mkInfo.info,

            }, filename))
            .then(function() {
                console.log('done');
            })
            .catch(function(err) {
                console.error(err);
            });
        //formData = {
        //name: filepath,
        //url: mkInfo.url,
        //info: mkInfo.info',

        //file: fs.createReadStream(filepath)
        //}

    } else {
        var i = process.argv.indexOf('-f');
        if (i == -1 || i + 1 >= process.argv.length) {
            console.error('file argument missed');
            return false;
        }
        var filepath = process.argv[i + 1];
        console.log(filepath);
        i = process.argv.indexOf('-url');
        if (i == -1 || i + 1 >= process.argv.length) {
            console.error('url argument missed');
            return false;
        }
        var url = process.argv[i + 1];

        console.log('url', url);
        return upload({
                name: filepath,
                url: url,
                info: 'uploaded',
            }, filepath)
            //var form = req.form();
            //form.append('file', fs.createReadStream(filepath));
    }

});

function upload(data, filename) {
    return function() {
        var promise = new Promise(function(resolve, reject) {

            console.log('URL', URL, host + '/auth/local')
            var options = {
                    url: host + '/auth/local',
                    method: 'POST',
                    json: true,
                    data: {
                        body: JSON.stringify({
                            'email': 'test@test.com',
                            'password': 'test'
                        })
                    }
                }
                //data.file = fs.createReadStream('dist/' + filename);

            var req = request(options);

            //req.post(URL, {
            //formData: data

            //}, function(err, resp, body) {

            //if (err) {
            //reject(err);
            //} else {
            //resolve(body);
            //}
            //});
        });


        return promise;
    }
}
