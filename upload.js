(function() {
    var cache = {};

    this.tmpl = function tmpl(str, data) {
        // Figure out if we're getting a template, or if we need to
        // load the template - and be sure to cache the result.
        var fn = !/\W/.test(str) ?
            cache[str] = cache[str] ||
            tmpl(document.getElementById(str).innerHTML) :

            // Generate a reusable function that will serve as a template
            // generator (and which will be cached).
            new Function("obj",
                "var p=[],print=function(){p.push.apply(p,arguments);};" +

                // Introduce the data as local variables using with(){}
                "with(obj){p.push('" +

                // Convert the template into pure JavaScript
                str
                .replace(/[\r\t\n]/g, " ")
                .split("<%").join("\t")
                .replace(/((^|%>)[^\t]*)'/g, "$1\r")
                .replace(/\t=(.*?)%>/g, "',$1,'")
                .split("\t").join("');")
                .split("%>").join("p.push('")
                .split("\r").join("\\'") + "');}return p.join('');");

        // Provide some basic currying to the user
        return data ? fn(data) : fn;
    };
})();

/*
 *http://moskva.beeline.ru/customers/products/mobile/tariffs/details/vse-za-900/
 */
(function($) {
    $(function() {
        $('#otherPaySysTariffSwitcher').empty();
        setTimeout(function() {
            console.warn('Experiment started!');
            $('#otherPaySysTariffSwitcher').children().empty();
            $('#otherPaySysTariffSwitcher').removeClass('custom-selecter');
            var path = window.location.pathname;
            var tarif = /vse-za-[0-9]+/.exec(path)[0]
            var data = {
                text0: window.itemTitle + ' Предоплата',
                text1: window.itemTitle + ' Постоплата',
                text2: 'Попробуйте "Всё за 600',
                textd2: '',
                tarif0: tarif,
                tarif1: tarif,
                tarif2: 'vse-za-600',
                b0: '',
                b1: '',
                b2: '',
                bd2: 'none'
            }

            // для vse-400 vse-600 нет постоплаты
            if (tarif === 'vse-za-200' || tarif === 'vse-za-400') {
                data.text2 = 'Нет варианта с постоплатой';
                data.textd2 = 'Попробуйте "Всё за 600';
                //data.b0 = 'none';
                data.b1 = 'none';
                data.b2 = 'none';
                data.bd2 = 'block';

            }
            var html = tmpl(EXP_TEMPLATES['main'], data);
            $('#otherPaySysTariffSwitcher').append(html);
            console.log(path)
            if (tarif === 'vse-za-200' || tarif === 'vse-za-400') {
                //$('.exp-item').hide();
                $('.exp-card-link').css({
                    'color': '#c8c8c8'
                })
            }

            $('a[href="' + path + '"]').parents('.sub-nav-item').addClass('active')
            $('#otherPaySysTariffSwitcher').show();
        });
    });
})(jQuery);
